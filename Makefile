
VERSION=3.4

build:
	docker build -t ferranp/tryton:${VERSION} .

rebuild:
	docker build --no-cache -t ferranp/tryton:${VERSION} .

run:
	docker run -d -p 18000:8000 -p 18002:8002 --env-file=.env ferranp/tryton:${VERSION}

runi:
	docker run -t --rm -i -p 18000:8000 -p 18002:8002 --env-file=.env ferranp/tryton:${VERSION}

runshell:
	docker run -t --rm -i -p 18000:8000 -p 18002:8002 --env-file=.env ferranp/tryton:${VERSION} /bin/bash

shell:
	docker run -t -i --env-file=.env --rm ferranp/tryton:${VERSION} /bin/bash

update_db:
	docker run -t --rm -i --env-file=.env ferranp/tryton:${VERSION} /app/run.sh -d ${DB} --all
