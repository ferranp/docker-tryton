#!/bin/sh

export TRYTOND_CONFIG=${TRYTOND_CONFIG:-/etc/trytond.conf}

exec /app/trytond/bin/trytond -v $@
