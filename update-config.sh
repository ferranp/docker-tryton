#!/bin/sh

BRANCH=`hg branch`

hg clone -b $BRANCH  https://bitbucket.org/nantic/tryton-config tmp

rm tmp/.hg -rf
rm tmp/local.cfg

sed -i -e "s/ssh:\/\/hg@/https:\/\//" tmp/*.cfg

rm config/ -rf
mv tmp/ config/
